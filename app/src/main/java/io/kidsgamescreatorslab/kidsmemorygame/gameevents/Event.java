package io.kidsgamescreatorslab.kidsmemorygame.gameevents;


public interface Event {

	String getType();
	
}
