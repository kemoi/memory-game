package io.kidsgamescreatorslab.kidsmemorygame.gameevents;

public abstract class AbstractEvent implements Event {

	protected abstract void fire(EventObserver eventObserver);

}
