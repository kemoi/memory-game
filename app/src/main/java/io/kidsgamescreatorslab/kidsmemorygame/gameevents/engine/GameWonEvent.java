package io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.AbstractEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventObserver;
import io.kidsgamescreatorslab.kidsmemorygame.gamemodel.GameState;

public class GameWonEvent extends AbstractEvent {

	public static final String TYPE = GameWonEvent.class.getName();

	public GameState gameState;

	
	public GameWonEvent(GameState gameState) {
		this.gameState = gameState;
	}

	@Override
	protected void fire(EventObserver eventObserver) {
		eventObserver.onEvent(this);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
