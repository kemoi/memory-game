package io.kidsgamescreatorslab.kidsmemorygame.gameevents;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.FlipDownCardsEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.GameWonEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.HidePairCardsEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.BackGameEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.DifficultySelectedEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.FlipCardEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.NextGameEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.ResetBackgroundEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.StartEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.ThemeSelectedEvent;


public interface EventObserver {

	void onEvent(FlipCardEvent event);

	void onEvent(DifficultySelectedEvent event);

	void onEvent(HidePairCardsEvent event);

	void onEvent(FlipDownCardsEvent event);

	void onEvent(StartEvent event);

	void onEvent(ThemeSelectedEvent event);

	void onEvent(GameWonEvent event);

	void onEvent(BackGameEvent event);

	void onEvent(NextGameEvent event);

	void onEvent(ResetBackgroundEvent event);

}
