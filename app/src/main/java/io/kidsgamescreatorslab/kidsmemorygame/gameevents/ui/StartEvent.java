package io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.AbstractEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventObserver;

/**
 * When the 'back to menu' was pressed.
 */
public class StartEvent extends AbstractEvent {



	public static final String TYPE = StartEvent.class.getName();

	@Override
	protected void fire(EventObserver eventObserver) {


		eventObserver.onEvent(this);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
