package io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.AbstractEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventObserver;
import io.kidsgamescreatorslab.kidsmemorygame.gamethemes.Theme;

public class ThemeSelectedEvent extends AbstractEvent {

	public static final String TYPE = ThemeSelectedEvent.class.getName();
	public final Theme theme;

	public ThemeSelectedEvent(Theme theme) {
		this.theme = theme;
	}

	@Override
	protected void fire(EventObserver eventObserver) {
		eventObserver.onEvent(this);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
