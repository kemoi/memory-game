package io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.AbstractEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventObserver;

public class FlipDownCardsEvent extends AbstractEvent {

	public static final String TYPE = FlipDownCardsEvent.class.getName();

	public FlipDownCardsEvent() {
	}
	
	@Override
	protected void fire(EventObserver eventObserver) {
		eventObserver.onEvent(this);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
