package io.kidsgamescreatorslab.kidsmemorygame.commonclasses;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;

import io.kidsgamescreatorslab.kidsmemorygame.gameengine.Engine;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventBus;

public class Shared {

	public static Context context;
	public static FragmentActivity activity; // it's fine for this app, but better move to weak reference
	public static Engine engine;
	public static EventBus eventBus;

}
