package io.kidsgamescreatorslab.kidsmemorygame.gamemodel;

import io.kidsgamescreatorslab.kidsmemorygame.gamethemes.Theme;

public class Game {


	public BoardConfiguration boardConfiguration;

	/**
	 * The board arrangment
	 */
	public BoardArrangment boardArrangment;

	/**
	 * The selected theme
	 */
	public Theme theme;

	public GameState gameState;

}
