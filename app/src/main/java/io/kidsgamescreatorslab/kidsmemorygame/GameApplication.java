package io.kidsgamescreatorslab.kidsmemorygame;

import android.app.Application;

import io.kidsgamescreatorslab.kidsmemorygame.gameutils.FontLoader;

public class GameApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		FontLoader.loadFonts(this);

	}
}
