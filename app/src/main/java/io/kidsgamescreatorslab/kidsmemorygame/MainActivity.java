package io.kidsgamescreatorslab.kidsmemorygame;


import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.widget.ImageView;

import io.kidsgamescreatorslab.kidsmemorygame.commonclasses.Shared;
import io.kidsgamescreatorslab.kidsmemorygame.gameengine.Engine;
import io.kidsgamescreatorslab.kidsmemorygame.gameengine.ScreenController;
import io.kidsgamescreatorslab.kidsmemorygame.gameengine.ScreenController.Screen;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventBus;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.BackGameEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameui.PopupManager;
import io.kidsgamescreatorslab.kidsmemorygame.gameutils.Utils;

public class MainActivity extends FragmentActivity {

	private ImageView mBackgroundImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Shared.context = getApplicationContext();
		Shared.engine = Engine.getInstance();
		Shared.eventBus = EventBus.getInstance();

		setContentView(R.layout.game_activity_main);
		mBackgroundImage = (ImageView) findViewById(R.id.background_image);

		Shared.activity = this;
		Shared.engine.start();
		Shared.engine.setBackgroundImageView(mBackgroundImage);

		// set background
		setBackgroundImage();

		// set menu
		ScreenController.getInstance().openScreen(Screen.MENU);


	}

	@Override
	protected void onDestroy() {
		Shared.engine.stop();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (PopupManager.isShown()) {
			PopupManager.closePopup();
			if (ScreenController.getLastScreen() == Screen.GAME) {
				Shared.eventBus.notify(new BackGameEvent());
			}
		} else if (ScreenController.getInstance().onBack()) {
			super.onBackPressed();
		}
	}

	private void setBackgroundImage() {
		Bitmap bitmap = Utils.scaleDown(R.drawable.background, Utils.screenWidth(), Utils.screenHeight());
		bitmap = Utils.crop(bitmap, Utils.screenHeight(), Utils.screenWidth());
		bitmap = Utils.downscaleBitmap(bitmap, 2);
		mBackgroundImage.setImageBitmap(bitmap);
	}

}
