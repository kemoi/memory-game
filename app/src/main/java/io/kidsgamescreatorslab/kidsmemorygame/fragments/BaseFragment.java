package io.kidsgamescreatorslab.kidsmemorygame.fragments;

import androidx.fragment.app.Fragment;

import io.kidsgamescreatorslab.kidsmemorygame.gameevents.EventObserver;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.FlipDownCardsEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.GameWonEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.engine.HidePairCardsEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.BackGameEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.FlipCardEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.NextGameEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.ResetBackgroundEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.ThemeSelectedEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.DifficultySelectedEvent;
import io.kidsgamescreatorslab.kidsmemorygame.gameevents.ui.StartEvent;

public class BaseFragment extends Fragment implements EventObserver {

	@Override
	public void onEvent(FlipCardEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(DifficultySelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(HidePairCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(FlipDownCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(StartEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ThemeSelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(GameWonEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(BackGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(NextGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ResetBackgroundEvent event) {
		throw new UnsupportedOperationException();
	}

}
